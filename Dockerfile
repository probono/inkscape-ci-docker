FROM ubuntu:18.04
RUN apt-get update -yqq && apt-get install -y -qq \
# Build Tools
  build-essential \
  wget \
  cmake \
  intltool \
  pkg-config \
  python-dev \
  libtool \
  ccache \
  doxygen \
  git \
  clang \
  clang-format \
  clang-tidy \
# Libraries
  libart-2.0-dev \
  libaspell-dev \
  libboost-dev \
  libcdr-dev \
  libgc-dev \
  libgdl-3-dev \
  libglib2.0-dev \
  libgnomevfs2-dev \
#  libgsl-dev \
#  libgsl0-dev \
  libgtk-3-dev \
  libgtkmm-3.0-dev \
  libgtkspell-dev \
  libjemalloc-dev \
  liblcms2-dev \
  libmagick++-dev \
  libpango1.0-dev \
  libpng-dev \
  libpoppler-glib-dev \
  libpoppler-private-dev \
  libpopt-dev \
  libpotrace-dev \
  librevenge-dev \
  libsigc++-2.0-dev \
  libsoup2.4-dev \
  libvisio-dev \
  libwpg-dev \
  libxml-parser-perl \
  libxml2-dev \
  libxslt1-dev \
  libyaml-dev \
  python-lxml \
  zlib1g-dev \
# Test Tools
  google-mock \
  imagemagick \
  libgtest-dev \
  fonts-dejavu \
&& apt-get clean && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*
